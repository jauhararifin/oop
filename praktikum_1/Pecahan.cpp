#include "Pecahan.h"

int __gcd(int a, int b) {
	if (b == 0) return a;
	return __gcd(b, a % b);
}

Pecahan::Pecahan() {
	(*this) = Pecahan(1,0,1);
}

Pecahan::Pecahan(int n, int a, int b) {
	this->n = n + a/b;
	this->a = a % b;
	this->b = b;
	
	int gcd = __gcd(this->a,this->b);
	this->a /= gcd;
	this->b /= gcd;
	
	if (this->a == 0)
		this->b = 1;
}

int Pecahan::getBulat () {
	return this->n;
}
int Pecahan::getPembilang () {
	return this->a;
}
int Pecahan::getPenyebut() {
	return this->b;
}
void Pecahan::setBulat(int _n) {
	this->n = _n;
}
void Pecahan::setPembilang(int _a){ 
	this->a = _a;
}
void Pecahan::setPenyebut(int _b) {
	this->b = _b;
}

bool Pecahan::isValid (int n, int a, int b) {
	return n >= 0 && a >= 0 && b >= 1;
}

Pecahan Pecahan::addWithThis (Pecahan P) {
	int n = this->n + P.n;
	
	int gcd = __gcd(this->b, P.b);
	int lcm = this->b * P.b / gcd; 
	
	int a = this->a * (lcm / this->b) + P.a * (lcm / P.b);
	int b = lcm;
	return Pecahan(n,a,b);
}

void Pecahan::accumulate(Pecahan P) {
	Pecahan tmp = this->addWithThis(P);
	this->n = tmp.getBulat();
	this->a = tmp.getPembilang();
	this->b = tmp.getPenyebut();
}

Pecahan Pecahan::add(Pecahan P1, Pecahan P2) {
	return P1.addWithThis(P2);
}
bool Pecahan::isEQMe(Pecahan P) {
	return this->n == P.n && this->a == P.a && this->b == P.b;
}
bool Pecahan::isGTMe(Pecahan P) {
	if (P.getBulat() == this->n)
		return P.getPembilang()*this->b > P.getPenyebut()*this->a;
	return P.getBulat() > this->n;
}
bool Pecahan::isLTMe(Pecahan P) {
	return !this->isEQMe(P) && !this->isGTMe(P);
}
float Pecahan::value() {
	return (float) this->n + (float) this->a / (float) this->b;
}

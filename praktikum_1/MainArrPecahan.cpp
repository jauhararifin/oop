#include <iostream>
#include "Pecahan.h"

using namespace std;

int main() {
	int n;
	
	cin>>n;
	Pecahan *ArrPecahan = new Pecahan[n];
	Pecahan sum(0,0,1),max,min;
	
	for (int i = 0; i < n; i++) {
		int x,y,z;
		cin>>x>>y>>z;
		ArrPecahan[i] = Pecahan(x,y,z);
	}
	
	max = ArrPecahan[0];
	min = ArrPecahan[0];
	for (int i = 0; i < n; i++) {
		sum.accumulate(ArrPecahan[i]);
		if (max.isGTMe(ArrPecahan[i]))
			max = ArrPecahan[i];
		if (min.isLTMe(ArrPecahan[i]))
			min = ArrPecahan[i];
	}
	
	cout<<sum.getBulat()<<" "<<sum.getPembilang()<<" "<<sum.getPenyebut()<<endl;
	for (int i = 0; i < n/2; i++) {
		Pecahan tmp = Pecahan::add(ArrPecahan[i], ArrPecahan[n-1-i]);
		cout<<tmp.getBulat()<<" "<<tmp.getPembilang()<<" "<<tmp.getPenyebut()<<endl;
	}
	if (n % 2 == 1)
		cout<<ArrPecahan[n/2].getBulat()<<" "<<ArrPecahan[n/2].getPembilang()<<" "<<ArrPecahan[n/2].getPenyebut()<<endl;
	cout<<max.getBulat()<<" "<<max.getPembilang()<<" "<<max.getPenyebut()<<endl;
	cout<<min.getBulat()<<" "<<min.getPembilang()<<" "<<min.getPenyebut()<<endl;
	
	return 0;
}

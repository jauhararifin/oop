/*
 * Nama			: Jauhar Arifin
 * NIM			: 13515049
 * Tanggal		: Kamis, 2 Februari 2017
 * Nama File	: Tabel.cpp
 * Deskripsi	: Implementasi tabel.h
 */

#include "Tabel.h" 

/*
 * Constructor tabel, membentuk tabel dengan jumlah elemen default
 */
Tabel::Tabel() {
	this->size = Tabel::defaultSize;
	this->data_ = new int[this->size];
	for (int i = 0; i < this->size; i++)
		this->data_[i] = Tabel::Nil;
}

/*
 * Constructor tabel, membentuk tabel dengan jumlah elemen sesuai parameter
 */
Tabel::Tabel(int size) {
	this->size = size;
	this->data_ = new int[this->size];
	for (int i = 0; i < this->size; i++)
		this->data_[i] = Tabel::Nil;
}

/*
 * Copy constructor tabel, membentuk tabel baru berdasarkan nilai dari tabel
 * yang sudah ada
 */
Tabel::Tabel(const Tabel& tab) {
	this->size = tab.size;
	this->data_ = new int[this->size];
	for (int i = 0; i < this->size; i++)
		this->data_[i] = tab.data_[i];
} 

/*
 * Destructor tabel, mendealokasi array yang telah dibuat oleh tabel
 */
Tabel::~Tabel() {
	delete this->data_;
}

/*
 * Operator =, mirip seperti copy constructor tabel
 */
Tabel& Tabel::operator=(const Tabel& tab) {
	Tabel *newtab = new Tabel(tab.size);
	for (int i = 0; i < newtab->GetSize(); i++)
		newtab->GetData()[i] = tab.data_[i];
	
	this->size = newtab->GetSize();
	this->data_ = newtab->GetData();
		
	return *newtab;
}

/*
 * Getter size dari tabel
 */
int Tabel::GetSize() {
	return this->size;
}

/*
 * Getter data dari tabel
 */
int* Tabel::GetData() {
	return this->data_;
}

/*
 * IsEmpty, mengecek apakah tabel kosong (Nil semua nilainya)
 */
bool Tabel::IsEmpty() {
	bool empty = true;
	for (int i = 0; i < this->size && empty; i++)
		if (this->data_[i] != Tabel::Nil)
			empty = false;
	return empty;
}

/*
 * IsFull, mengecek apakah tabel penuh (tidak ada yang Nil nilainya)
 */
bool Tabel::IsFull() {
	bool full = true;
	for (int i = 0; i < this->size && full; i++)
		if (this->data_[i] == Tabel::Nil)
			full = false;
	return full;
}                  

/*
 * Add, mengisi tabel dengan nilai x dari parameter
 */
void Tabel::Add(int x) {
	bool found = false;
	for (int i = 0; i < this->size && !found; i++)
		if (this->data_[i] == Tabel::Nil) {
			this->data_[i] = x;
			found = true;
		}
}

/*
 * Del, mengembalikan nilai tabel pada index ke i, dan menghapus nilainya
 */
int Tabel::Del(int i) {
	int tmp = this->data_[i];
	this->data_[i] = Tabel::Nil;
	return tmp;
}

/*
 * DelX, menghapus nilai x pertama dari tabel
 */
void Tabel::DelX(int x) {
	bool found = false;
	for (int i = 0; i < this->size && !found; i++)
		if (this->data_[i] == x) {
			this->data_[i] = Tabel::Nil;
			found = true;
		}
}

/*
 * NbElmt, mengembalikan jumlah elemen dalam tabel
 */
int Tabel::NbElmt() {
	int n = 0;
	for (int i = 0; i < this->size; i++)
		if (this->data_[i] != Tabel::Nil)
			n++;
	return n;
}

/*
 * Sum, menjumlahkan semua elemen dalam tabel, lalu mengembalikan hasilnya
 */
int Tabel::Sum() {
	int sum = 0;
	for (int i = 0; i < this->size; i++)
		if (this->data_[i] != Tabel::Nil)
			sum += this->data_[i];
	return sum;
}

/*
 * Or, mengembalikan true jika terdapat elemen 1 pada tabel
 */
bool Tabel::Or() {
	bool hasil = false;
	for (int i = 0; i < this->size && !hasil; i++)
		if (this->data_[i] == 1)
			hasil = true;
	return hasil;
}

/*
 * Accumulate, membuat tabel baru berisi satu elemen yang merupakan
 * penjumlahan dari tabel sekarang
 */
Tabel Tabel::Accumulate() {
	Tabel* tab = new Tabel(1);
	tab->Add(this->Sum());
	return *tab;
}

/*
 * IsEq, mengecek apakah dua buah tabel berisi elemen yang sama
 */
bool Tabel::IsEq(Tabel t) {
	if (this->size != t.GetSize())
		return false;
		
	bool equal = true;
	for (int i = 0; i < this->size && equal; i++)
		if (this->data_[i] != t.GetData()[i])
			equal = false;
	return equal;
}

/*
 * IsBit, mengecek apakah elemen tabel merupakan 1, 0 atau Nil
 */
bool Tabel::IsBit(int i) {
	return this->data_[i] == 0 || this->data_[i] == 1 || this->data_[i] == Tabel::Nil;
}

#include "kamus.h"
#include "tabel_generik.h"
#include <iostream>

using namespace std;

Kamus::Kamus(int size) : size(size) {
  this->t_key = new TabelGenerik<int>(nil_key, this->size);
  this->t_value = new TabelGenerik<string>(nil_value, this->size);
}

Kamus::~Kamus() {
  delete this->t_key;
  delete this->t_value;
}

void Kamus::Print() {
  for (int i = 0; i < this->size; i++)
    if (this->t_key->GetData()[i] != nil_key)
      cout<<this->t_value->GetData()[i]<<endl;
}

void Kamus::Add(int k, string v) {
  this->t_key->Add(k);
  this->t_value->Add(v);
}

void Kamus::Add(int i, int k, string v) {
  int idx = i;
  while (this->t_key->GetData()[idx] != nil_key)
    idx = (idx + 1) % this->size;
  this->t_key->GetData()[idx] = k;
  this->t_value->GetData()[idx] = v;
}

void Kamus::Del(int k) {
  for (int i = 0; i < this->size; i++)
    if (this->t_key->GetData()[i] == k) {
      this->t_key->Del(i);
      this->t_value->Del(i);
    }
}

void Kamus::Del(string v) {
  for (int i = 0; i < this->size; i++)
    if (this->t_value->GetData()[i] == v) {
      this->t_key->Del(i);
      this->t_value->Del(i);
    }
}

void Kamus::Del(int k, string v) {
  for (int i = 0; i < this->size; i++)
    if (this->t_key->GetData()[i] == k && this->t_value->GetData()[i] == v) {
      this->t_key->Del(i);
      this->t_value->Del(i);
    }
}

int Kamus::Search(int k, string v) {
  int idx = -99;
  for (int i = 0; i < this->size && idx == -99; i++)
    if (this->t_key->GetData()[i] == k && this->t_value->GetData()[i] == v)
      idx = i;
  return idx;
}

TabelGenerik<int>* Kamus::GetKeyData() {
  return this->t_key;
}

TabelGenerik<string>* Kamus::GetValueData() {
  return this->t_value;
}
/**
  * @file tabel_generik.h
  * @brief blblblblb
  *
  * @author Jauhar Arifin
  * @date   2/17/2017
  */

#ifndef TABEL_H
#define TABEL_H

#include <iostream>

using namespace std;

/** 
  * @class TabelGenerik
  * Kelas TabelGenerik menyimpan data dengan tipe tertentu dalam bentuk tabel.
  * Data yang disimpan akan diletakkan pada sebuah array linier.Array yang
  * menyimpan data tidak selalu kontigu. Penghapusan data tidak menjamin array
  * yang tercipta menjadi kontigu. Data yang masuk akan diletakkan di tempat yang
  * kosong pada tabel, sedangkan penghapusan data hanya akan menghapus data di
  * posisi yang bersangkutan tanpa menggeser data tabel.
  * Elemen yang disimpan oleh tabel dapat bertipe apapun.
  */
template <typename T>
class TabelGenerik {

   public:
      /**
        * Digunakan untuk menciptakan tabel dengan mengalokasikan memori untuk objek
        * sebesar nilai default dan nilai tabel diinisialisasi dengan nil
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @param nil_value menyetakan nilai nil dari objek yang disimpan
        * di dalam tabel
        */
      TabelGenerik(T nil_value);

      /**
        * Digunakan untuk menciptakan tabel dengan mengalokasikan memori untuk objek
        * sebesar paramter yang diberikan dan nilai tabel diinisialisasi dengan nil
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @param nil_value menyatakan nilai nil dari objek yang disimpan
        * di dalam tabel
        * @param size menyatakan ukuran tabel yang ingin dibuat
        */
      TabelGenerik(T nil_value, int size);

      /**
        * Copy contructor untuk tabel
        * @author Jauhar Arifin
        * @date 2/17/2017
        */
      TabelGenerik(const TabelGenerik&);

      /**
        * Melakukan dealokasi terhadap memori yang dialokasi ketika instance diciptakan.
        * @author Jauhar Arifin
        * @date 2/17/2017
        */
      ~TabelGenerik();

      /**
        * Digunakan untuk melakukan assignment tabel terhadap tabel lain.
        * @author Jauhar Arifin
        * @date 2/17/2017
        */
      TabelGenerik& operator=(const TabelGenerik&);

      /**
        * Digunakan untuk mendapatkan ukuran tabel sekarang.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @return int yang menyatakan ukuran tabel sekarang.
        */
      int  GetSize();
      
      /**
        * Digunakan untuk mendapatkan array linier yang menyimpan data tabel sekarang.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @return int* yang menyatakan array linier yang menyimpan data tabel sekarang.
        */
      T* GetData();

      /**
        * Digunakan untuk menentukan apakah tabel sedang kosong atau tidak.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @return bool yang menyatakan apakah tabel kosong.
        */
      bool IsEmpty();

      /**
        * Digunakan untuk menentukan tabel sedang penuh.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @return bool yang menyatakan apakah tabel penuh.
        */
      bool IsFull(); 

      /**
        * Digunakan untuk memasukkan sebuah nilai kedalam tabel. Nilai yang dimasukkan
        * akan diletakkan pada array linier dengan indeks terkecil yang nilainya masih
        * Nil.
        * @pre Tabel tidak boleh penuh
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @param x meyatakan nilai yang ingin dimasukkan
        */
      void Add(T x);

      /**
        * Digunakan untuk mendapatkan nilai data tabel pada posisi tertentu lalu
        * menghapusnya dari tabel.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @param i menyatakan posisi data yang akan diambil dan dihapus.
        * @return data yang berada di posisi ke-i.
        */
      T Del(int i);

      /**
        * Digunakan untuk menghapus data dari tabel yang memiliki nilai x dengan
        * indeks paling kecil.
        * @pre X tidak bernilai Nil
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @param x meyatakan nilai yang ingin dihapus.
        */
      void DelX(T x);

      /**
        * Digunakan untuk mendapatkan jumlah elemen yang berada dalam tabel.
        * @pre Tabel tidak boleh penuh
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @return int yang menyatakan jumlah elemen yang ada di dalam tabel.
        */
      int NbElmt(); 

      /**
        * Digunakan untuk menentukan apakah dua buah tabel sama.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @param t yang menyatakan tabel yang ingin dibandingkan.
        * @return bool yang menyatakan apakah kedua tabel sama.
        */
      bool IsEq(TabelGenerik<T> t);

      /**
        * Digunakan untuk mendapatkan jumlah instance yang terbentuk dari kelas ini.
        * @author Jauhar Arifin
        * @date 2/17/2017
        * @return int yang menytakan jumlah instance yang terbentuk dari kelas ini.
        */
      static int NumberTabelGenerik();

   private:
      static int number_tab;
      
      const T Nil;
      const int defaultSize= 50;
      
      int size;
      T* data_;
};

template <typename T>
int TabelGenerik<T>::number_tab = 0; 

template <typename T>
TabelGenerik<T>::TabelGenerik(T nil_value) : TabelGenerik(nil_value, defaultSize)  {
}

template <typename T>
TabelGenerik<T>::TabelGenerik(T nil_value, int size) : Nil(nil_value)  {
   this->size = size;
   this->data_ = new T[this->size];
   for (int i = 0; i < this->size; i++)
      this->data_[i] = this->Nil;

   TabelGenerik<T>::number_tab++;
}

template <typename T>
TabelGenerik<T>::TabelGenerik(const TabelGenerik& t) : Nil(t.Nil) {
   this->size = size;
   this->data_ = new T[this->size];
   for (int i = 0; i < this->size; i++)
      this->data_[i] = t.data_[i];

   TabelGenerik<T>::number_tab++;
}

template <typename T>
TabelGenerik<T>::~TabelGenerik() {
   delete [] this->data_;
   TabelGenerik<T>::number_tab--;
}

template <typename T>
TabelGenerik<T>& TabelGenerik<T>::operator=(const TabelGenerik<T>& t) {
   for (int i = 0; i < min(this->size, t->size); i++)
      this->data_[i] = t->data_[i];
   return *this;
}

template <typename T>
int TabelGenerik<T>:: GetSize() {
   return this->size;
}

template <typename T>
T* TabelGenerik<T>::GetData() {
   return this->data_;
}

template <typename T>
bool TabelGenerik<T>::IsEmpty() {
   bool empty = true;
   for (int i = 0; i < this->size && empty; i++)
      if (this->data_[i] != Nil)
         empty = false;
   return empty;
}

template <typename T>
bool TabelGenerik<T>::IsFull() {
   bool full = true;
   for (int i = 0; i < this->size && full; i++)
      if (this->data_[i] == Nil)
         full = false;
   return full;
} 

template <typename T>
void TabelGenerik<T>::Add(T x) {
   int i = 0;
   while (i < this->size && this->data_[i] != Nil)
      i++;
   if (i < this->size)
      this->data_[i] = x;
}

template <typename T>
T TabelGenerik<T>::Del(int i) {
   T temp = this->data_[i];
   this->data_[i] = Nil;
   return temp;
}

template <typename T>
void TabelGenerik<T>::DelX(T x) {
   bool deleted = false;
   for (int i = 0; i < this->size && !deleted; i++)
      if (this->data_[i] == x) {
         deleted = true;
         this->data_[i] = Nil;
      }
}

template <typename T>
int TabelGenerik<T>::NbElmt() {
   int count = 0;
   for (int i = 0; i < this->size; i++)
      count += (int) (this->data_[i] != Nil);
   return count;
}

template <typename T>
bool TabelGenerik<T>::IsEq(TabelGenerik<T> t) {
   if (this->size == t.size) {
      bool same = true;
      for (int i = 0; i < this->size && same; i++)
         same = same && (this->data_[i] == t.data_[i]);
      return same;
   } else
      return false;
}

template <typename T>
int TabelGenerik<T>::NumberTabelGenerik() {
   return TabelGenerik<T>::number_tab;
}


#endif

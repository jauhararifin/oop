# Soal 1
## Constructor
Untuk membuat constructor pada kelas matriks, pertama-tama harus di set dulu nilai ```n_brs```, dan ```n_kol``` nya karena kedua nilai ini bersifat constant, jadi waktu pertama kali insance dari matriks dibuat, nilainya harus langsung didefinisikan. Caranya yang paling gampang adalah dengan menuliskan kode seperti berikut :

    Matriks::Matriks() : n_brs(DEFSIZE), n_kol(DEFSIZE)
    Matriks::Matriks(int n, int m) : n_brs(n), n_kol(m)
Untuk copy constructor juga sama sih

    Matriks::Matriks(const Matriks& m) : n_brs(m.n_brs), n_kol(m.n_kol)
    
Abis ngisi ```n_brs``` dan ```n_kol```, sekarang yang perlu dilakukan adalah alokasi memori untuk isi matriksnya di variabel ```b```. Variabel ```b``` itu harus nampung nilai dari elemen matriks, jadi variabel ```b``` harus jadi semacam array 2 dimensi gitu. Makannya variabel ```b```-nya bertipe ```int**```.
Jadi variabel ```b``` itu pointer yang berisi pointer ke integer. Atau bisa dibilang juga berisi array of (pointer to integer). Kalau kita punya variabel ```int* x``` artinya kan ```x``` itu array of integer, kalau punya ```int** b``` berarti ```b``` itu array of ```int*``` (atau dengan kata lain array of pointer_of_integer).
Waktu pertama kali dibuat kan, variabel ```b``` isinya belum ada, jadi harus harus dialokasi memori dulu pake ```new```, caranya gini:
    
    b = new int*[n_brs]
Nah, abis dialokasi, sekarang ```b``` nilainya udah ada tuh, ada sebanyak ```n_brs``` slot untuk nampung ```int*```. Jadi sekarang kita punya ```n_brs``` buah slot kosong untuk nampung pointer to integer. Langkah selanjutnya, alokasi tiap slot kosong ini untuk ```n_kol``` buah integer. Kalau ```b``` itu tipenya ```int**```, maka ```b[0],b[1],b[2]...``` itu tipenya ```int*``` (pointer to integer) atau dengan kata lain array of integer. Analoginya sama kaya "kalau ```string* x```, ```x``` itu array of string, maka ```x[1]``` itu stringnya.
Jadi selanjutnya untuk alokasi b, caranya di for aja

    for (int i = 0; i < n_brs; i++)
        b[i] = new int[n_kol];
Abis itu tinggal diinisialisasi aja nilainya jadi 0 semua
    
    for (int i = 0; i < n_brs; i++)
		for (int j = 0; j < n_kol; j++)
			b[i][j] = 0;
Tapi untuk copy constructor inisialisasi nilainya bukan 0 sih, tapi nilai yang mau di-copy.

# Destructor
Buat destruktor gampang sih, tinggal delete-delete aja, tapi gabisa langsung ```delete b``` karena nanti ```b[0],b[1],b[2],...```nya nggak ke-delete. Jadi harus satu-satu, isinya ```b``` didelete dulu, baru ```b```nya didelete.

    Matriks::~Matriks() {
	    for (int i = 0; i < n_brs; i++)
		    delete b[i];
	    delete b;
    }
    
## Operator =
Buat ```operator=``` sih tinggal copy aja, jangan lupa return sih.

    Matriks& Matriks::operator= (Matriks& m) {
	    for (int i = 0; i < m.n_brs; i++)
		    for (int j = 0; j < m.n_kol; j++)
			    b[i][j] = m.b[i][j];
	    return (*this);
    }
   
## Operator +
Buat ```operator+``` karena di header bukan fungsi friend, jadi ini fungsi yang dimiliki setiap instance dari matriks. Jadi fungsi ```Matriks& Matriks::operator+ (Matriks m2)``` itu nambahin matriks ```this``` dengan matriks ```m2``` dan hasilnya matriks baru dan harus di return.
Caranya kita buat matriks barunya dulu, ukurannya tentu saja dari ukuran ```this``` sama ```m2``` yang paling maksimum. Untuk ngitung ukurannya bisa gini

    int n = this->n_brs < m2.n_brs ? m2.n_brs : this->n_brs;
	int n = this->n_kol < m2.n_kol ? m2.n_kol : this->n_kol;
Terus buat matriksnya
    
	Matriks *hasil = new Matriks(n, m);
Matriksnya harus dibuat pake ```new```, gabisa dibuat pake cara ```Matriks hasil(n,n)```, karena kalau kaya gitu nanti jadinya matriks hasil yang kebuat bersifat lokal. Kalo lokal artinya kan disimpen di stack, kalau disimpen di stack nanti waktu fungsinya keluar kan stacknya ancur, variabelnya ikut ancur tuh jadi bakal runtime error. Intinya kalo mau return objek dari fungsi sih objeknya harus dinamik, jadi bikinnya harus pake ```new```.
Setelah itu copy isi dari matriks ```this``` sama ```m2``` ke matriks baru
    
    for (int i = 0; i < this->n_brs; i++)
		for (int j = 0; j < this->n_kol; j++)
			hasil->b[i][j] = this->b[i][j];
			
	for (int i = 0; i < m2.n_brs; i++)
		for (int j = 0; j < m2.n_kol; j++)
			hasil->b[i][j] = m2.b[i][j];
Sekarang bagian matriks yang nggak ikut dijumlahin udah berisi nilai dari matriks penyusunnya. Langkah selanjutnya adalah njumlahin matriks yang berpotongan. Caranya tinggal ambil n_brs dan n_kol dari potongan matriks itu. Gampang sih tinggal cari minimum baris sama minimum kolom dari matriks penyusunnya. Contohnya kalau matriks 2x5 ditambahin matriks 7x3 nanti matriks yang berpotongannya jadi 2x3. Terus jangan lupa di-return.
    
    n = this->n_brs > m2.n_brs ? m2.n_brs : this->n_brs;
	m = this->n_kol > m2.n_kol ? m2.n_kol : this->n_kol;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			hasil->b[i][j] = this->b[i][j] + m2.b[i][j];
	return *hasil;
	
## Operator *
```operator*``` sih sama, tapi karena dia function yang ada diluar kelas itu, jadi nggak kaya ```operator+``` yang njumlahin ```this``` sama ```m1```. ```Matriks& operator* (const Matriks& m1, const Matriks& m2)``` itu njumlahin ```m1``` sama ```m2```. Caranya sama kaya ```operator+``` sih, cuman ```this```nya diganti ```m1```. Terus tambahnya diganti kali.
    
## Operator <<
Operator << itu sebenernya apasih? Sebenernya itu sih ya operator biasa selayaknya operator lain, cuma ```operator<<``` seringnya dipake buat nulis output. waktu kita nulis ```cout<<"hello world"``` itu artinya kan nulis output ke layar "hello wold". Nah disini itu tu sebenernya behind the scenenya kaya gini : ```operator<<(cout, "Hello World")```, jadi parameter pertamanya itu menyatakan kemana kita mau nulis, ```cout``` ini adalah variabel yang menyatakan standard output atau dalam hal ini adalah layar. Kita bisa juga isi pake variabel yang isinya file, jadi nanti ```filenya<<"Hello world"```. ```Cout``` ini sebenerya tipenya adalah ```ostream``` singkatan dari output stream (kayanya) yang menandakan kemana kita mau output.
Terus kalau ```cout<<"Hello "<<"world"``` itu gimana, kok bisa berantai kaya gitu? Nah sebenernya fungsi dari ```operator<<(ostream, ...)``` itu return valuenya juga ```ostream``` lagi. Jadi operasi ```cout<<"Hello "``` = ```operator<<(cout, "Hello")``` itu return-nya atau nilainya adalah ```cout``` lagi. Jadi ```cout<<"Hello "<<"world"``` itu sebenarnya sama aja kaya gini

    operator<<(operator<<(cout, "Hello "), "World")
Karena ```operator<<("Hello ")``` nilainya adalah ```cout```, maka ```operator<<(operator<<(cout, "Hello "), "World")``` nilainya adalah ```operator<<(cout, "World")``` . Oleh karena itulah makannya ```operator<<``` bisa berantai.

Nah kita tinggal output-output aja untuk nyelesaiin soal ini. Tapi outputnya bukan pake ```cout``` melainkan pakai paramter yang disediain di fungsi, yaitu ```os```. Terus di akhir jangan lupa return lagi ```os```nya biar bisa berantai.
    
    ostream& operator<<(ostream& os, const Matriks& m) {
	    os<<"N = "<<m.n_brs<<endl;
	    os<<"M = "<<m.n_kol<<endl;
	    for (int i = 0; i < m.n_brs; i++) {
		    for (int j = 0; j < m.n_kol - 1; j++)
			    os<<m.b[i][j]<<" ";
	    	os<<m.b[i][m.n_kol-1]<<endl;
	    }
	    return os;
    }
Jadi nanti di program utama kita, kalau mau nyetak matriks, bisa tinggal kaya gini.
    
    Matriks a(5,5);
    cout<<a<<endl;
Itu nanti nyetak ke layar, bisa juga kalau kita punya variabel yang menyatakan output file, misalnya namanya ```file```, nanti bisa juga langsung ```file<<a<<endl```, nanti nyetaknya ke file.
    
## IsEqSize, Setter dan Getter
Terlalu mudah untuk dijelaskan

# Soal 2
## Constructor Dan Destruktor
Untuk kelas special matriks, karena dia merupakan turunan dari kelas matriks, jadi kita bisa pakai konstruktornya kelas matriks aja. Kelas special matriks kan nggak perlu perlakuan khusus di konstruktornya (konstruktornya sama persis kaya kelas matriks), jadi kita bisa langsung pakai kelas matriksnya aja. Destruktornya juga sama aja sih.

    SpecialMatriks::SpecialMatriks() : Matriks() {}
    SpecialMatriks::SpecialMatriks(int n) : Matriks(n,n) {}
    SpecialMatriks::SpecialMatriks(const SpecialMatriks& sm) : Matriks(sm) {}
    SpecialMatriks::~SpecialMatriks() {}
    
## Operator+ dan Operator*
Karena kelas special matriks itu ukuran kolom sama barisnya pasti sama, jadi untuk ```operator+``` sama ```operator*``` operasinya lebih sederhana daripada kelas matriks biasa.
Idenya sama sih, pertama bikin dulu objek special matriks baru yang ukurannya diambil dari ukuran terbesar dari penyusunnya.

    int n = this->n_brs < m2.n_brs ? m2.n_brs : this->n_brs;
    int m = this->n_brs > m2.n_brs ? m2.n_brs : this->n_brs;
    SpecialMatriks *hasil = new SpecialMatriks(n);
Variabel ```n``` itu menunjukkan ukuran matriks yang besar. Variabel ```m``` menunjukkan ukuran matriks yang kecil.
Terus tinggal diisi nilai nya. Pakai for aja. Kalau baris dan kolom yang mau kita isi masih berada dibawah m (masih di matriks yang kecil), nilainya tinggal ditambahin atau dikaliin. Kalau udah diluar range, tinggal dicopy dari matriks yang besar.

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
		    if (i < m && j < m) // kalau masih di range matriks kecil
			    hasil->b[i][j] = this->b[i][j] | m2.b[i][j]; // buat operator+
			    hasil->b[i][j] = this->b[i][j] & m2.b[i][j]; // buat operator*
			else // kalau udah diluar range matriks kecil
				hasil->b[i][j] = this->n_brs < m2.n_brs ? m2.b[i][j] : this->b[i][j];
Terus tinggal direturn. ```return *hasil```.
    
## Virtual Function
Untuk fungsi virtual, yang harus dijadikan virtual adalah ```operator+``` dan ```SetData``` karena kedua fungsi itu implementasinya beda antara kelas matriks dan kelas special matriks. Jadi tinggal ubah ```Matriks.h```, dari
    
    Matriks& operator+ (Matriks m2);
    void SetData (int i, int j, int v);
Jadi
    
    virtual Matriks& operator+ (Matriks m2);
    virtual void SetData (int i, int j, int v);
Untuk ```operator*``` nggak bisa dijadiin virtual karena bukan fungsi dari kelas, tapi fungsi bebas. Menurutku emang agak aneh sih ini.
    
## Operator=, Getter, dan Setter
Caranya sama aja kaya kelas matriks. ```operator=``` tinggal copy isinya aja. Getter dan setter tinggal return sama set aja.

# Soal 3
Untuk soal 3, catatan aja, itu soalnya kayanya kurang lengkap sih. Format output matriksnya bukan kaya gini

    N = 5
    M = 5
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    
Tapi langsung isi matriksnya kaya gini

    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0

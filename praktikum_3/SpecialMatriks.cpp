#include <iostream>
#include "Matriks.h"
#include "SpecialMatriks.h"

using namespace std;

SpecialMatriks::SpecialMatriks() : Matriks() {
}

SpecialMatriks::SpecialMatriks(int n) : Matriks(n,n) {
}

SpecialMatriks::SpecialMatriks(const SpecialMatriks& sm) : Matriks(sm) {
}

SpecialMatriks::~SpecialMatriks() {
}

SpecialMatriks& SpecialMatriks::operator= (SpecialMatriks& sm) {
	for (int i = 0; i < this->n_brs; i++)
		for (int j = 0; j < this->n_brs; j++)
			this->b[i][j] = sm.b[i][j];
	
	return (*this);
}
    
SpecialMatriks& SpecialMatriks::operator+ (SpecialMatriks m2) {
	int n = this->n_brs < m2.n_brs ? m2.n_brs : this->n_brs;
	int m = this->n_brs > m2.n_brs ? m2.n_brs : this->n_brs;
	
	SpecialMatriks *hasil = new SpecialMatriks(n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (i < m && j < m)
				hasil->b[i][j] = this->b[i][j] | m2.b[i][j];
			else
				hasil->b[i][j] = this->n_brs < m2.n_brs ? m2.b[i][j] : this->b[i][j];
	
	return *hasil;
}

SpecialMatriks& operator* (const SpecialMatriks& sm1, const SpecialMatriks& sm2) {
	int n = sm1.n_brs < sm2.n_brs ? sm2.n_brs : sm1.n_brs;
	int m = sm1.n_brs > sm2.n_brs ? sm2.n_brs : sm1.n_brs;
	
	SpecialMatriks *hasil = new SpecialMatriks(n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (i < m && j < m)
				hasil->b[i][j] = sm1.b[i][j] & sm2.b[i][j];
			else
				hasil->b[i][j] = sm1.n_brs < sm2.n_brs ? sm2.b[i][j] : sm1.b[i][j];
	
	return *hasil;
}

void SpecialMatriks::SetData(int i, int j, int v) {
	if (v == 1)
		this->b[i][j] = v;
	else
		this->b[i][j] = 0;
}
